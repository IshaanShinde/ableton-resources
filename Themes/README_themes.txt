For Windows:

1. Go to the following path

C:\ProgramData\Ableton\Live 11 Lite\Resources\Themes

Make sure you have show hidden files on in case you're manually going through the folders

2. Copy the .ask file into the Themes Folder

You can then select it from you preferences -> Look Feel -> Theme

For MAC:

1. Go to your Applications Folder
2. Right Click the Ableton application
3. Select "Show Package Contents"
4. Go to Contents -> App Resources -> Themes
5. Copy the .ask file here;

You can then select it from you preferences -> Look Feel -> Theme